<?php include('./inc/header.php'); ?>

  <div class="slider">
    <?php foreach ($page->slider as $slide): ?>
      <div class='slide' style='background-image:url(<?=$slide->image->url?>)'>
           <div class='slidecontent'>
               <h1>Solar Dolar Wolar Woot</h1>
           </div>
       </div>
    <?php endforeach; ?>
  </div>
  <article class="home">
    <?=$page->body?>
  </article>
<?php include('./inc/footer.php'); ?>
