<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="<?=$config->urls->templates?>images/mini_logo.png">

    <title></title>
    <meta name="description" content="">

    <link rel="stylesheet" href="<?=$config->urls->templates?>sass/styles.min.css">
    <link rel="stylesheet" href="<?=$config->urls->templates?>sass/jquery.fancybox.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?=$config->urls->templates?>scripts/main.js"></script>
    <script src="<?=$config->urls->templates?>scripts/jquery.fancybox.min.js"></script>

  </head>
  <body class="wrapper">
    <header>
      <div class="header-logo">
        <a href="<?=$pages->get('/')->url?>"><img src="<?=$config->urls->templates?>images/logo2.png" alt=""></a>
      </div>
      <nav>
        <div class="nav-toggler">
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div class="search"><form action="<?=$pages->get(1043)->url?>" method="GET"><input type="text" name="search" value=""></form></div>
        <ul class="lang-menu">
          <li><a href="#">LV</a></li>
          <li><a href="#">RU</a></li>
          <li class="open"><a href="#">EN</a></li>
        </ul>
        <ul class="main-menu">
          <?php foreach ($pages->get('/')->children()->prepend($pages->get('/')) as $child):?>
              <?php if ($page === $child || $page->parent === $child && $page->parent !== $pages->get('/')): ?>
                <li class="open">
                  <a href="<?=$child->url?>"><?=$child->title?></a>
                  <?php if ($child->hasChildren() && $child !== $pages->get('/')): ?>
                    <ul>
                      <?php foreach ($child->children as $c): ?>
                        <li><a href="<?=$c->url?>"><?=$c->title?></a></li>
                      <?php endforeach; ?>
                    </ul>
                  <?php endif; ?>
                </li>
              <?php else: ?>
                <li>
                  <a href="<?=$child->url?>"><?=$child->title?></a>
                  <?php if ($child->hasChildren() && $child !== $pages->get('/')): ?>
                    <ul>
                      <?php foreach ($child->children as $c): ?>
                        <li><a href="<?=$c->url?>"><?=$c->title?></a></li>
                      <?php endforeach; ?>
                    </ul>
                  <?php endif; ?>
                </li>
              <?php endif; ?>

        <?php endforeach; ?>
        </ul>
        <ul class="sub-menu">
          <?php foreach ($pages->get('/')->children as $child) : ?>
            <?php if ($child->hasChildren()): ?>
              <li>
                <a href="<?=$child->url?>"><?=$child->title?></a>
                <ul>
                  <?php foreach ($child->children as $c):?>
                    <?php if ($c === $page): ?>
                      <li class='open'><a href="<?=$c->url?>"><?=$c->title?></a></li>
                    <?php else: ?>
                      <li><a href="<?=$c->url?>"><?=$c->title?></a></li>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </ul>
              </li>
            <?php endif;?>
          <?php endforeach; ?>
        </ul>
      </nav>
      <div class="clear"></div>
    </header>
    <main>
