<?php include('./inc/header.php'); ?>
<article class="single">
  <?=$page->body?>
</article>
<div class="sub-pages">
  <?php foreach($page->children as $child): ?>
    <article>
      <div class="article-image" style="background-image:url(<?=$child->image->url?>)"></div>
      <div class="article">
        <h2><?=$child->title?></h2>
        <?=$child->desc?>
        <div class="more"><a href="<?=$child->url?>"><?=__('read more')?></a></div>
      </div>
      <div class="clear"></div>
    </article>
  <?php endforeach;?>
</div>
<?php include('./inc/footer.php'); ?>
