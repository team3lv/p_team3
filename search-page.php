<?php include('./inc/header.php');

if ($input->get->search) {
  $search_phrase = $sanitizer->text($input->get->search);
  if ($search_phrase) {
    $results = $pages->find("template=single-page|single-sub-page, title|body|places.body*=$search_phrase");
  }
}

?>
<div class="seach-results">
  <h1><?=__('Search for content in go-parikrama.com')?></h1>
  <form action="#" method="get">
    <input type="text" name="search" value="<?php if ($input->get->search) echo $input->get->search; ?>" placeholder="<?=__('Search phrase')?>">
    <input type="submit" name="" value="<?=__('Search')?>">
  </form>
  <div class="search-header">Results of "<?=$input->get->search?>":</div>
  <?php foreach ($results as $p):?>
    <article>
        <div class="title">
          <?=$p->parent->title?> &#xbb; <a href="<?=$p->url?>"> <?=$p->title?> </a>
        </div>
        <?php
          $sentances = explode('.', $p->body);
          foreach ($sentances as $s) {
            if (strpos($s, $search_phrase) !== false) {
                echo str_replace($search_phrase, "<b>".$search_phrase."</b>",$s) . "<br><br>";
            }

          }
        ?>
    </article>
  <?php endforeach; ?>
</div>

<?php include('./inc/footer.php') ?>
