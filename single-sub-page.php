<?php include ('./inc/header.php'); ?>
<article>
  <h1><?=__('About')?> <?=$page->title?></h1>
  <?=$page->body?>
</article>
<div class="places"><?=__('Places in ')?> <?=$page->title?></div>
<div class="sub-items">
  <?php foreach ($page->places as $place):?>
    <article>
      <div class="places-header" style="background-image:url(<?=$place->images->first()->url?>)">
        <?php foreach ($place->images as $image): ?>
          <a href="<?=$image->url?>" data-fancybox="<?=$place->id?>"></a>
        <?php endforeach; ?>
      </div>
      <h2><?=$place->title?></h2>
      <?=$place->body;?>
    </article>
  <?php endforeach;?>
</div>

<?php include ('./inc/footer.php'); ?>
