<?php include('./inc/header.php');

$error = '';

if ($input->post->send) {
  $name = $sanitizer->text($input->post->name);
  $email = $sanitizer->email($input->post->email);
  $text = $sanitizer->text($input->post->text);

  $lastname_robot_trap = $sanitizer->text($input->post->lastname);



  if (!$name) {
    $error .= __('Fill name field correcly!');
  }
  if (!$email) {
    $error .= __('Fill email field correcly!');
  }
  if (!$text) {
    $error .= __('Fill text field correcly!');
  }

  if ($name && $email && $text && !$lastname_robot_trap) {
    $emailto = 'randdomrobert@gmail.com';
    $mailer = wireMail();
    $mailer->to($emailto);
    $mailer->from($email);
    $mailer->subject("[Go-parikrama.com] Contact form");
    $mailer->body("
      Name : $name
      Email : $email
      Message : $text
    ");

    $error .= $mailer->send();

  }
  if ($lastname_robot_trap) {
    $error = 1;
  }
}

?>

<div id="map"></div>

<script>
  function initMap() {
    var styles = [
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e9e9e9"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dedede"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#333333"
                },
                {
                    "lightness": 40
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f2f2f2"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        }
    ]

    var center = {lat: 54.7991788, lng: 57.1645697};
    var logo_position = {lat: 56.8087725, lng: 24.0510226 }
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 3,
      icon: '<?=$config->urls->templates?>images/favicon.png',
      styles: styles,
      center: center
    });
    var marker = new google.maps.Marker({
      position: logo_position,
      map: map
    });
  }
</script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDX_B2QG2Vw8sAi6CPl2Cpp-BEkTkNqO7s&callback=initMap">
</script>
<?php if ($error): ?>
  <div class="success">
    <h4><?=__('Thank you!')?></h4>
    <?=__('Message has been send to ')?> <?=$emailto?>
  </div>
<?php else: ?>
  <?php if ($error > 1):?>
    <div class="error">
      <h4><?=__('Errors found in form!')?></h4>
      <?=$error?>
    </div>
  <?php endif; ?>
  <form class="contact-us" action="#" method="post">
    <h1><?=$error?><?=__('Contact us')?></h1>
    <input class="a2" type="text" name="name" value="" placeholder="<?=__('Name')?>*" required>
    <input class="a7" type="text" name="lastname" value="" placeholder="<?=__('Lastname')?>*">
    <input class="a3" type="email" name="email" value="" placeholder="<?=__('Email')?> *" required>
    <textarea class="a4" name="text" rows="4" required placeholder="<?=__('Your text')?>"></textarea>
    <input type="submit" name="send" value="<?=__('send')?>">
  </form>
<?php endif; ?>

<?php include('./inc/footer.php');?>
